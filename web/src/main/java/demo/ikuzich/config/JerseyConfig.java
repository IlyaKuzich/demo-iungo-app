package demo.ikuzich.config;

import demo.ikuzich.controller.HealthCheckController;
import demo.ikuzich.controller.UserController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        //Validation
        register(ValidationConfigurationContextResolver.class);

        //Controllers
        register(HealthCheckController.class);
        register(UserController.class);
    }
}
