package demo.ikuzich.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("demo.ikuzich")
public class IungoDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(IungoDemoApplication.class, args);
    }
}
