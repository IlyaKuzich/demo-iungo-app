package demo.ikuzich.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaveUserDto {
    private String firstName;
    private String lastName;
    private String email;
}
