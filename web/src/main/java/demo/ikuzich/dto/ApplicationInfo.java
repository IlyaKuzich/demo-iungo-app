package demo.ikuzich.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ApplicationInfo {
    private String version;
    private String message;
}
