package demo.ikuzich.controller;

import demo.ikuzich.dto.SaveUserDto;
import demo.ikuzich.dto.UserDto;
import demo.ikuzich.model.User;
import demo.ikuzich.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static demo.ikuzich.helper.ModelDtoHelper.mergeModel;
import static demo.ikuzich.helper.ModelDtoHelper.toDto;
import static demo.ikuzich.helper.ModelDtoHelper.toDtos;
import static demo.ikuzich.helper.ModelDtoHelper.toModel;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GET
    public List<UserDto> findAll() {
        return toDtos(userService.findAll());
    }

    @GET
    @Path("/{id}")
    public UserDto find(@PathParam("id") Long id) {
        User user = userService.find(id);
        if (user == null) {
            throw new NotFoundException();
        }
        return toDto(userService.find(id));
    }

    @POST
    public UserDto create(@Valid SaveUserDto saveUserDto) {
        return toDto(userService.save(toModel(saveUserDto)));
    }

    @PUT
    @Path("/{id}")
    public UserDto update(@PathParam("id") Long id, @Valid SaveUserDto saveUserDto) {
        User user = userService.find(id);
        if (user == null) {
            throw new NotFoundException();
        }
        return toDto(userService.save(mergeModel(saveUserDto, user)));
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
        User user = userService.find(id);
        if (user == null) {
            throw new NotFoundException();
        }
        userService.delete(id);
    }
}
