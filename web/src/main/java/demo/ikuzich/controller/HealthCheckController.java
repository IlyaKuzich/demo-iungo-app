package demo.ikuzich.controller;
import demo.ikuzich.dto.ApplicationInfo;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/healthCheck")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class HealthCheckController {

    @GET
    public ApplicationInfo healthCheck() {
        return ApplicationInfo.builder()
                .message("Iungo demo app is running")
                .version("1.0")
                .build();
    }
}
