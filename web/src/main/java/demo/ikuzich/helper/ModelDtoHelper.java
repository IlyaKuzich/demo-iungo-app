package demo.ikuzich.helper;

import demo.ikuzich.dto.SaveUserDto;
import demo.ikuzich.model.User;
import demo.ikuzich.dto.UserDto;

import java.util.List;
import java.util.stream.Collectors;

abstract public class ModelDtoHelper {

    public static UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        return userDto;
    }

    public static List<UserDto> toDtos(List<User> users) {
        return users.stream()
                .map(ModelDtoHelper::toDto)
                .collect(Collectors.toList());
    }

    public static User toModel(SaveUserDto saveUserDto) {
        User user = new User();
        user.setFirstName(saveUserDto.getFirstName());
        user.setLastName(saveUserDto.getLastName());
        user.setEmail(saveUserDto.getEmail());
        return user;
    }

    public static User mergeModel(SaveUserDto saveUserDto, User user) {
        user.setFirstName(saveUserDto.getFirstName());
        user.setLastName(saveUserDto.getLastName());
        user.setEmail(user.getEmail());
        return user;
    }
}
