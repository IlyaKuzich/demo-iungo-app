CREATE DATABASE IF NOT EXISTS `demo_iungo` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

USE `demo_iungo`;

CREATE TABLE `user` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE = InnoDB;
