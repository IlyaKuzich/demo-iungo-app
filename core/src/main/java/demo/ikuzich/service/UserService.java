package demo.ikuzich.service;

import demo.ikuzich.model.User;

import java.util.List;

public interface UserService {

    User find(Long id);

    List<User> findAll();

    User save(User user);

    void delete(Long id);
}
